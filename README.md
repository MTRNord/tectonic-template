# Template

## How to clone this?

Use `git clone --recursive --depth 1 --shallow-submodules https://gitlab.com/MTRNord/latex-snippets.git`

## How to install tectonic

1. Install biber
2. Install rust
3. Install tectonic via `cargo install --git https://github.com/tectonic-typesetting/tectonic.git tectonic`

## How to build pdf

Run `tectonic -X build`

## How to update the snippets folder

Just run `git submodule update --remote snippets` in the root folder.